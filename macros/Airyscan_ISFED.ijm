var m_batch;
var m_reg_stack;
var m_original_stack;
var m_e;
var m_smooth;

macro "Airyscan_ISFED"
{
	requires("1.53d");
	// settings dialog
	Dialog.create("Airyscan Processing");
	Dialog.addHelp("file:///" + getDirectory("plugins") + "AiryscanJ" + File.separator + "doc" + File.separator + "index.html"); 

	Dialog.addMessage("       Airyscan: ISFED reconstruction", 18, "#0071C3");
	if ( nImages == 0 ){
		Dialog.addMessage("WARNING: No image open, using batch mode", 9, "#FFA500");
	}
	
	// registration
	Dialog.addMessage("__________________________________________________", 14, "#0071C3");
	Dialog.addMessage("RECONSTRUCTION:", 14, "#0071C3");
	default_batch = true;
	if ( nImages > 0 ){
		Dialog.addImageChoice("       Original stack");
		Dialog.addImageChoice("       Co-registered stack");  
		default_batch = false;
	}
	
  	Dialog.addNumber("          N:", 7, 0, 18, "");
  	Dialog.addChoice("          eps", newArray("auto", 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5), "auto");
	Dialog.addCheckbox("Smooth", false);

 	// batch processing
 	Dialog.addMessage("\n");
 	Dialog.addMessage("__________________________________________________", 14, "#0071C3");
 	Dialog.addMessage("BATCH PROCESSING:", 14, "#0071C3");
 	Dialog.addCheckbox("Batch", default_batch);
 	Dialog.show();

 	// get settings
 	if ( nImages > 0 ){
 		m_original_stack = Dialog.getImageChoice();
 		m_reg_stack = Dialog.getImageChoice();
 	}
 	m_N = Dialog.getNumber();
 	m_e = Dialog.getChoice();
 	m_smooth = Dialog.getCheckbox();
  	m_batch = Dialog.getCheckbox();

setBatchMode(true);
	if (m_batch){
		run("Close All");
		original_dir = getDirectory("Original images directory");
		reg_dir = getDirectory("Co-registered images directory");
		outputdir = getDirectory("output directory");
		original_files = getFileList(original_dir);
		reg_files = getFileList(reg_dir);
		for (i=0 ; i < files.length ; i++)
		{
			if (endsWith(".czi") )
			{
				open(original_dir + original_files[i]);
				m_original_stack = getTitle();
				open(reg_dir + reg_files[i]);
				m_reg_stack = getTitle();
				process();
				saveAs("TIFF", files[i]);
				run("Close All");
			}
		}
	}
	else{
		process();
	}
setBatchMode(false);	
}

function process()
{
	selectWindow(m_original_stack);
	//run("32-bit");
	selectWindow(m_reg_stack);
	run("32-bit");
	getDimensions(width, height, channels, slices, frames);
	print("Stack dimensions:", width);
	print("Stack dimensions:", height);
	print("Stack dimensions:", channels);
	print("Stack dimensions:", slices);
	print("Stack dimensions:", frames);

	if (slices > 1 && frames > 1){
		channel_str = "";
		for (c = 1; c <= channels; c++) {
			eps = m_e;
			if (m_e == "auto"){
				selectWindow(m_original_stack);
				run("Duplicate...", "duplicate channels="+c+" slices="+slices/2);
				sure_stack_original = getTitle();
				selectWindow(m_reg_stack);
				run("Duplicate...", "duplicate channels="+c+" slices="+slices/2);
				sure_stack_reg = getTitle();
				run("Concatenate...", "  image1=["+sure_stack_original+"] image2=["+sure_stack_reg+"]");
				sure_stack = getTitle();
				eps = sure_guided(sure_stack);
				close(sure_stack);
			}
			channel_str += "c" + toString(c) + "=channel"+toString(c) + " "; 
			for (s = 1; s <= slices; s++) {
				selectWindow(m_original_stack);
				Stack.setChannel(c);
				Stack.setSlice(s);
				run("Duplicate...", "duplicate channels="+c+" slices="+s);
				im = getTitle();
				selectWindow(m_reg_stack);
				Stack.setChannel(c);
				Stack.setSlice(s);
				run("Duplicate...", "duplicate channels="+c+" slices="+s);
				reg_im = getTitle();
				ISFED_stack(reg_im, im, eps);
				rename("IFED_channel"+toString(c)+"_"+toString(s));
				close(im);
				close(reg_im);
			}
			run("Images to Stack", "name=channel"+c+" title=[IFED_] use");
		}
		channel_str += "create";
		if (channels > 1){
			run("Merge Channels...", channel_str);
		}
		rename("ISFED_" + m_original_stack);
	}
	else{
		channel_str = "";
		for (c = 1; c <= channels; c++) {
			eps = m_e;
			if (m_e == "auto"){
				selectWindow(m_original_stack);
				run("Duplicate...", "duplicate channels="+c+" slices=1");
				sure_stack_original = getTitle();
				selectWindow(m_reg_stack);
				run("Duplicate...", "duplicate channels="+c+" slices=1");
				sure_stack_reg = getTitle();
				run("Concatenate...", "  image1=["+sure_stack_original+"] image2=["+sure_stack_reg+"]");
				sure_stack = getTitle();
				eps = sure_guided(sure_stack);
				close(sure_stack);
			}
			print("Process channel:", c);
			channel_str += "c" + toString(c) + "=channel"+toString(c) + " "; 
			selectWindow(m_original_stack);
			Stack.setChannel(c);
			run("Duplicate...", "duplicate channels="+c);
			rename("im");
			im = getTitle();
			selectWindow(m_reg_stack);
			Stack.setChannel(c);
			run("Duplicate...", "duplicate channels="+c);
			rename("reg_im");
			reg_im = getTitle();
			ISFED_stack(reg_im, im, eps);
			close(im);
			close(reg_im);
			rename("channel"+toString(c));
		}
		if (channels > 1){
			channel_str += "create";
			run("Merge Channels...", channel_str);
		}
		rename("ISFED_" + m_original_stack);
	}
	if (channels > 1){
		Stack.setDisplayMode("color");
	}
}

function ISFED_stack(reg_im, im, eps){

	// ISM
	selectWindow(reg_im);
	run("32-bit");
	run("Z Project...", "projection=[Sum Slices]");
	rename("ISM");

	// Wide Field
	selectWindow(im);
	run("32-bit");
	run("Z Project...", "projection=[Sum Slices]");
	rename("widefield");

	// SFED1 = ISM - q*widefield
	selectWindow("widefield");
	run("Multiply...", "value="+eps);
	imageCalculator("Subtract create 32-bit", "ISM","widefield");

	// set negative values to zero
	for (x=0 ; x < getWidth(); x++){
		for (y=0 ; y < getHeight() ; y++){
			if ( getPixel(x, y) < 0){
				setPixel(x, y, 0);
			}
		}
	}
	close("ISM");
	close("widefield");
}

function sure_guided(stackTitle){
	selectWindow(stackTitle);
	run("32-bit");
	tmpPath = getDirectory("plugins");
	saveAs("tiff", tmpPath + "tmp.tif");
	exe_name = "sure";
	if (startsWith(getInfo("os.name"), "Windows")) {
		exe_name += ".exe";
	}
	execPath = getDirectory("plugins") + "AiryscanJ"+File.separator+exe_name;
	exec(execPath, "-i", tmpPath + "tmp.tif", "-o", tmpPath + "tmp_sure.tif", "-refidx", 0, "-m1", 0, "-m2", 31, "-n1", 32, "-n2", 63, "-neigh", 11);

	open(tmpPath + "tmp_sure.tif");
	epsilonTitle = "epsilon";
	rename(epsilonTitle);
	run("Duplicate...", " ");
	close("tmp.tif");

	setAutoThreshold("Otsu dark");
	setOption("BlackBackground", false);
	run("Convert to Mask");
	run("Set Measurements...", "mean redirect="+epsilonTitle+" decimal=3");
	run("Clear Results"); 
	run("Analyze Particles...", "display clear");
	eps = 0.0;
	for (i = 0; i < nResults; i++) {
		eps += getResult("Mean", i);
	}
	eps /= nResults;
	print("eps=",eps);

	File.delete(tmpPath + "tmp.tif"); 
	File.delete(tmpPath + "tmp_deconv.tif");
	return eps;
}
