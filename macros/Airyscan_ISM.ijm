var m_batch;
var m_reg_stack;

macro "Airyscan_ISM"
{
    requires("1.53d");
	// settings dialog
	Dialog.create("Airyscan Processing");
	Dialog.addHelp("file:///" + getDirectory("plugins") + "AiryscanJ" + File.separator + "doc" + File.separator + "index.html"); 

	Dialog.addMessage("       Airyscan: ISM reconstruction", 18, "#0071C3");
	if ( nImages == 0 ){
		Dialog.addMessage("WARNING: No image open, using batch mode", 9, "#FFA500");
	}
	
	// registration
	Dialog.addMessage("__________________________________________________", 14, "#0071C3");
	Dialog.addMessage("RECONSTRUCTION:", 14, "#0071C3");
	default_batch = true;
	if ( nImages > 0 ){
		Dialog.addImageChoice("       Co-registered stack");  
		default_batch = false;
	}

 	// batch processing
 	Dialog.addMessage("\n");
 	Dialog.addMessage("__________________________________________________", 14, "#0071C3");
 	Dialog.addMessage("BATCH PROCESSING:", 14, "#0071C3");
 	Dialog.addCheckbox("Batch", default_batch);
 	Dialog.show();

 	// get settings
 	if ( nImages > 0 ){
 		m_reg_stack = Dialog.getImageChoice();
 	}
  	m_batch = Dialog.getCheckbox();

	if (m_batch){
		run("Close All");
		dir = getDirectory("input directory");
		outputdir = getDirectory("output directory");
		files = getFileList(dir);
		for (i=0 ; i < files.length ; i++){
			if (endsWith(".czi") ){
				open(dir + files[i]);
				m_reg_stack = getTitle();
				process();
				saveAs("TIFF", files[i]);
				run("Close All");
			}
		}
	}
	else{
		process();
	}
}

function process()
{
	selectWindow(m_reg_stack);
	imageTitle = getTitle();
	run("32-bit");
	getDimensions(width, height, channels, slices, frames);

	if (slices > 1 && frames > 1){
		run("Re-order Hyperstack ...", "channels=[Channels (c)] slices=[Frames (t)] frames=[Slices (z)]");
		run("Z Project...", "projection=[Sum Slices] all");
		run("Re-order Hyperstack ...", "channels=[Channels (c)] slices=[Frames (t)] frames=[Slices (z)]");
		Stack.setDisplayMode("color");
		rename("ISM_" + imageTitle);
		out_image = getTitle();

		// reset the original stack
		selectWindow(m_reg_stack);
		run("Re-order Hyperstack ...", "channels=[Channels (c)] slices=[Frames (t)] frames=[Slices (z)]");
		Stack.setDisplayMode("color");
		selectWindow(out_image);
	}
	else{
		run("Z Project...", "projection=[Sum Slices]");
		rename("ISM_" + imageTitle);
	}
}
