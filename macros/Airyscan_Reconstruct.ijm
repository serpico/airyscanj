var m_method;

macro "airyscanprocessing"{

	// settings dialog
	Dialog.create("Airyscan Processing");
	Dialog.addHelp("file:///" + getDirectory("plugins") + "AiryscanJ" + File.separator + "doc" + File.separator + "index.html"); 

	Dialog.addMessage("Fiji macro to reconstruct an image using the 32 channels \n (czi format) images from the Airyscan microscope");
	
	// registration
	Dialog.addMessage("__________________________________________________");
	Dialog.addMessage("RECONSTRUCTION:");
  	Dialog.addChoice("Method:", newArray("Pseudo confocal",  "ISM", "IFED", "ISFED", "Deconvolution"), "ISM");
  	Dialog.show();	

 	// get settings
 	m_method = Dialog.getChoice();
	
 	if (m_method == "Pseudo confocal"){
 		runMacro(getDirectory("plugins") + File.separator + "AiryscanJ" + File.separator + "Airyscan_PseudoConfocal.ijm");
 	}
 	else if (m_method == "ISM"){
 		runMacro(getDirectory("plugins") + File.separator + "AiryscanJ" + File.separator + "Airyscan_ISM.ijm");
 	}
 	else if (m_method == "IFED"){
 		runMacro(getDirectory("plugins") + File.separator + "AiryscanJ" + File.separator + "Airyscan_IFED.ijm");
 	}
 	else if (m_method == "ISFED"){
 		runMacro(getDirectory("plugins") + File.separator + "AiryscanJ" + File.separator + "Airyscan_ISFED.ijm");
 	}
 	else if (m_method == "Deconvolution"){
 		runMacro(getDirectory("plugins") + File.separator + "AiryscanJ" + File.separator + "Airyscan_Deconvolution.ijm");
 	}
}
