var m_batch;
var m_N;
var m_e;
var m_smooth;

macro "Airyscan_ISM"
{
    requires("1.53d");
	// settings dialog
	Dialog.create("Airyscan Processing");
	Dialog.addHelp("file:///" + getDirectory("plugins") + "AiryscanJ" + File.separator + "doc" + File.separator + "index.html"); 

	Dialog.addMessage("       Airyscan: IFED reconstruction", 18, "#0071C3");
	if ( nImages == 0 ){
		Dialog.addMessage("WARNING: No image open, using batch mode", 9, "#FFA500");
	}
	
	// registration
	Dialog.addMessage("__________________________________________________", 14, "#0071C3");
	Dialog.addMessage("RECONSTRUCTION:", 14, "#0071C3");
  	Dialog.addNumber("          N:", 7, 0, 18, "");
  	Dialog.addChoice("          eps", newArray("auto", 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5), "auto");
	Dialog.addCheckbox("Smooth", false);

 	// batch processing
 	Dialog.addMessage("\n");
 	Dialog.addMessage("__________________________________________________", 14, "#0071C3");
 	Dialog.addMessage("BATCH PROCESSING:", 14, "#0071C3");
 	default_batch = true;
	if ( nImages > 0 ){
		default_batch = false;
	}
 	Dialog.addCheckbox("Batch", default_batch);
 	Dialog.show();

 	// get settings
 	m_N = Dialog.getNumber();
 	m_e = Dialog.getChoice();
 	m_smooth = Dialog.getCheckbox();
  	m_batch = Dialog.getCheckbox();

setBatchMode(true);
	if (m_batch){
		run("Close All");
		dir = getDirectory("input directory");
		outputdir = getDirectory("output directory");
		files = getFileList(dir);
		for (i=0 ; i < files.length ; i++){
			if (endsWith(".czi") ){
				open(dir + files[i]);
				m_reg_stack = getTitle();
				process();
				saveAs("TIFF", files[i]);
				run("Close All");
			}
		}
	}
	else{
		process();
	}
setBatchMode(false);	
}

function process()
{
	imageTitle = getTitle();
	//run("32-bit");
	getDimensions(width, height, channels, slices, frames);

	if (slices > 1 && frames > 1){
		channel_str = "";
		for (c = 1; c <= channels; c++) {
			eps = m_e;
			if (m_e == "auto"){
				run("Duplicate...", "duplicate channels="+c+" slices="+slices/2);
				sure_stack = getTitle();
				eps = sure_guided(sure_stack);
				close(sure_stack);
			}
			channel_str += "c" + toString(c) + "=channel"+toString(c) + " "; 
			for (s = 1; s <= slices; s++) {
				selectWindow(imageTitle);
				Stack.setChannel(c);
				Stack.setSlice(s);
				run("Duplicate...", "duplicate channels="+c+" slices="+s);
				im = getTitle();
				IFED_stack(im, m_N, eps);
				rename("IFED_channel"+toString(c)+"_"+toString(s));
			}
			run("Images to Stack", "name=channel"+c+" title=[IFED_] use");
		}
		channel_str += "create";
		if (channels > 1){
			run("Merge Channels...", channel_str);
		}
		rename("IFED_" + imageTitle);
	}
	else{
		channel_str = "";
		for (c = 1; c <= channels; c++) {
			eps = m_e;
			if (m_e == "auto"){
				run("Duplicate...", "duplicate channels="+c+" slices=1");
				sure_stack = getTitle();
				eps = sure_guided(sure_stack);
				close(sure_stack);
			}
			channel_str += "c" + toString(c) + "=channel"+toString(c) + " "; 
			selectWindow(imageTitle);
			Stack.setChannel(c);
			run("Duplicate...", "duplicate channels="+c);
			im = getTitle();
			IFED_stack(im, m_N, eps);
			rename("channel"+toString(c));
		}
		if (channels > 1){
			channel_str += "create";
			run("Merge Channels...", channel_str);
		}
		rename("IFED_" + imageTitle);
	}
	if (channels > 1){
		Stack.setDisplayMode("color");
	}
}

function IFED_stack(imageTitle, N, q){

	// center 
	selectWindow(imageTitle);
	run("32-bit");
	run("Z Project...", "stop="+N+" projection=[Sum Slices]");
	rename("center");

	// ring
	selectWindow(imageTitle);
	run("32-bit");
	run("Z Project...", "start="+d2s(N+1,1)+" stop=32 projection=[Sum Slices]");
	rename("ring");

	// FED = center - q*ring
	selectWindow("ring");
	run("Multiply...", "value="+q);
	imageCalculator("Subtract create 32-bit", "center","ring");
	rename("IFED_" + imageTitle);

	// Set negative values to zero
	for (x=0 ; x < getWidth(); x++){
		for (y=0 ; y < getHeight() ; y++){
			if ( getPixel(x, y) < 0){
				setPixel(x, y, 0);
			}
		}
	}
	close("center");
	close("ring");	
	close(imageTitle);
	selectWindow("IFED_" + imageTitle);
	if (m_smooth){
		run("Gaussian Blur...", "sigma=0.5");
	}
}

function sure_guided(stackTitle){
	selectWindow(stackTitle);
	run("32-bit");
	tmpPath = getDirectory("plugins");
	saveAs("tiff", tmpPath + "tmp.tif");
	exe_name = "sure";
	if (startsWith(getInfo("os.name"), "Windows")) {
		exe_name += ".exe";
	}
	execPath = getDirectory("plugins") + "AiryscanJ"+File.separator+exe_name;
	exec(execPath, "-i", tmpPath + "tmp.tif", "-o", tmpPath + "tmp_sure.tif", "-refidx", 0, "-m1", 0, "-m2", m_N-1, "-n1", m_N, "-n2", 31, "-neigh", 11);

	open(tmpPath + "tmp_sure.tif");
	epsilonTitle = "epsilon";
	rename(epsilonTitle);
	run("Duplicate...", " ");
	close("tmp.tif");

	setAutoThreshold("Otsu dark");
	setOption("BlackBackground", false);
	run("Convert to Mask");
	run("Set Measurements...", "mean redirect="+epsilonTitle+" decimal=3");
	run("Clear Results"); 
	run("Analyze Particles...", "display clear");
	eps = 0.0;
	for (i = 0; i < nResults; i++) {
		eps += getResult("Mean", i);
	}
	eps /= nResults;
	print("eps=",eps);

	File.delete(tmpPath + "tmp.tif"); 
	File.delete(tmpPath + "tmp_deconv.tif");
	return eps;
}
