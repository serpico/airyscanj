/*=============================================================================

_Header

_Module_Name 	: MAD.h

_Directory	: 

_Description 	: Declaration de MAD.cc

_Calls   	:

_Authors	: Charles KERVRANN

_Contents       :

_Last Release   : 17/11/03

_end

=============================================================================*/






/*===========================================================================*/

#ifndef _MAD_H_
#define _MAD_H_

/*===========================================================================*/

#include "define.h"
#include "include.h"

/*===========================================================================*/

extern void  QSort(double *a,int left,int right);
extern int   compare(const void *i, const void *j);
extern void  Tri(float *tab, int N);
extern void WienerFiltering(float *I, float eta, float sigma, int patchsize, float *Iwiener, int w, int h);
extern void GaussianFiltering(float *I, float sigma, float *Igaussian, int w, int h);
extern void Median3H (float *I, float *Imed, int w, int h);
extern void Median3V (float *I, float *Imed, int w, int h);
extern void Median3D45 (float *I, float *Imed, int w, int h);
extern void Median3D135 (float *I, float *Imed, int w, int h);
extern void Median5HV(float *I, float *Imed, int w, int h);
extern void Median5D (float *I, float *Imed, int w, int h);
extern void Median9  (float *I, float *Imed, int w, int h);
extern void Peak9(float *I, float *var, float th, float *Ipeak, int w, int h);
extern void Median13 (float *I, float *Imed, int w, int h);

//extern float Tri(float *tab, int N, int Ncut);
//extern void  EM_2MG(float *tab, int N, float mu0, float mu1);
extern void  EM_2MG(float *tab, int N, float *MU0, float *VAR0, float *MU1, 
		    float *VAR1, float *pi0);
extern float MeanLTS(float *tab, int N);
extern float SigmaLTS(float *tab, int N);
extern float SigmaLMedS(float *tab, int N);
extern float SigmaLMS(float *tab, int N);
extern void  PseudoResidu(float *pixm, float *residu, int w, int h);
extern void  ResiduML(float *pixm, float *residu, int w, int h);
extern void  carte_MAD_gaussien(float *p_pixmap, float sig, float *sig_I, int p, int w, int h);
extern void  carte_MAD_gaussien_(float *p_pixmap, float sig, float *sig_I, int p, int w, int h);
extern void  carte_NoisevarianceML(float *p_pixmap, float sig, float *sig_I, int p, int w, int h, float gamma_p);
extern void  carte_NoiseVariance_PoissonGaussien (float *p_pixmap, float g0, float e_DC, float *sig_I, int p, int w, int h);
extern void  carte_MAD_speckle(float *p_pixmap, float sig, float *sig_I, int p, int w, int h);
extern void  carte_eta(float *p_pixmap, int w, int h, float sig, int p, int N, float *c_eta);
extern void  noise_linear_model(float *residu, float *pixmap, float *sig_eps, float *sig_eta, 
			       int p, int w, int h);
extern void carte_corr(float *ima_u, float *ima_v, int p, int w, int h, float *corr);
extern void carte_snr(float *ima_v, float sigma, int p, int w, int h, float *snr);
extern void QR_metrics(float *ima, int w, int h, int p, float *Q, float *R);
extern void Tensor(float *pixmap, int w, int h, int p, float *angle, float *anisotropy, float *valp1, float *valp2);

extern float MSE_Var_B2(float *Inoise, float *DN_I, int w, int h, int SigBlur, float *MSE);
extern float MSE_Var_B2_(float *Inoise, float *DN_I, int w, int h, int SigBlur, float *MSE);
extern void ApplyFilter2D(float *Iin, int w, int h, int index, float *filter, float *Iout);
extern void GaussFilter(float variance, int *index, float *filter);
extern void PeakFiltering(float *pixmap, int w, int h, double alpha, float *nopeak_pixmap); 

extern void WildBootstrap(float *I0, float *Istar, float *Istar_, int w, int h);
extern void WGNoise(float *I, float *Iwgn, float *noise, int w, int h, float sigma, float eps);
extern float comp_div(float *I, float *Ieps, float *noise, int w, int h, float eps);
extern void cov_I(float *ima_u, float *ima_v, int p, int w, int h, float *ima_cov);


/*===========================================================================*/

#endif

/*===========================================================================*/

