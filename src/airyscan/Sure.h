/// \file Sure.h
/// \brief Sure class
/// \author Charles Kervrann, Sylvain Prigent
/// \version 0.1
/// \date 2020

#pragma once

#include "airyscanExport.h"
#include "simage/SImageFloat.h"

class AIRYSCAN_EXPORT Sure{

public:
    Sure();

public:
    void setRef(SImageFloat* ref);
    void setA(SImageFloat* a);
    void setB(SImageFloat* b);

    void setWindowSize(int windowSize);
    void setThreshold(float threshold);

    SImageFloat* calculateEpsilonMap();
    void run();

    SImageFloat* epsilonMap();
    float epsilon();

protected:
    void AiryScan3DStat(float *IN_i, float &mean, float &mode, int w, int h);

    void run_local();
    void run_global();
    void run_localsum();

protected:
    SImageFloat* m_ref;
    SImageFloat* m_a;
    SImageFloat* m_b;

    int m_windowSize;
    float m_threshold;

    SImageFloat* m_epsilonMap;
    float m_epsilon;

};
