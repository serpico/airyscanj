/// \file Sure.cpp
/// \brief Sure class
/// \author Charles Kervrann, Sylvain Prigent
/// \version 0.1
/// \date 2018

#include "Sure.h"
#include <iostream>

#include "MAD.h"

#define MAX(X,Y)         ((X)>(Y) ? (X) : (Y))
#define MIN(X,Y)         ((X)<(Y) ? (X) : (Y))
#define INCLUS(X,Y,W,H)  (((X)>(-1))&&((X)<(W))&&((Y)>(-1))&&((Y)<(H)) ? 1 : 0)

Sure::Sure(){
    m_windowSize = 5;
    m_threshold = 0; // celegans=135, billes=17
}

void Sure::setRef(SImageFloat* ref){
    m_ref = ref;

    std::cout << "max ref = " << m_ref->getMax() << std::endl;
    std::cout << "min ref = " << m_ref->getMin() << std::endl;

}

void Sure::setA(SImageFloat* a){
    m_a = a;
}

void Sure::setB(SImageFloat* b){
    m_b = b;
}

void Sure::setWindowSize(int windowSize){
    m_windowSize = windowSize;
}

void Sure::setThreshold(float threshold){
    m_threshold = threshold;
}

void Sure::run(){
    std::cout << "-------------------------" << std::endl;
    std::cout << "Sure:" << std::endl;
    std::cout << "windows size = " << m_windowSize << std::endl;
    std::cout << "threshold = " << m_threshold << std::endl;
    this->run_local();
    this->run_global();
    this->run_localsum();
}

void Sure::run_global(){
    unsigned int sx = m_a->getSizeX();
    unsigned int sy = m_a->getSizeY();
    float* a_buffer = m_a->getBuffer();
    float* b_buffer = m_b->getBuffer();
    float* ref_buffer = m_ref->getBuffer();

    // calculate epsilon map
    m_epsilon = 0.;
    float tmp1 = 0.;
    float tmp2 = 0.;

    for (int x = 0 ; x < sx ; x++){
        for (int y = 0 ; y < sy ; y++){
            if (ref_buffer[sy*x+y] >= m_threshold){
                tmp1 += (ref_buffer[sy*x+y] - a_buffer[sy*x+y])*b_buffer[sy*x+y];
                tmp2 += b_buffer[sy*x+y]*b_buffer[sy*x+y];
            }
        }
    }

    if (tmp2 < 0.00000001){
        m_epsilon = 0.;
    }
    else{
        m_epsilon = - tmp1/tmp2;
    }

    std::cout << "epsilon global = " << m_epsilon << std::endl;
}

void Sure::run_localsum(){
    unsigned int sx = m_a->getSizeX();
    unsigned int sy = m_a->getSizeY();
    float* a_buffer = m_a->getBuffer();
    float* b_buffer = m_b->getBuffer();
    float* ref_buffer = m_ref->getBuffer();

    // calculate epsilon map
    m_epsilon = 0.;
    float numerator = 0.0;
    float denominator = 0.0;
    float tmp1, tmp2;
    for (int x = 0 ; x < sx ; x++){
        for (int y = 0 ; y < sy ; y++){
            if (ref_buffer[sy*x+y] >= m_threshold){
                tmp1 = 0.;
                tmp2 = 0.;
                for (int dx=-m_windowSize; dx<=m_windowSize; dx++){
                    for (int dy=-m_windowSize; dy<=m_windowSize; dy++){
                        if (INCLUS(x+dx,y+dy,sx,sy)!=0){
                            tmp1 += (ref_buffer[sy*(x+dx)+y+dy] - a_buffer[sy*(x+dx)+y+dy])*b_buffer[sy*(x+dx)+y+dy];
                            tmp2 += b_buffer[sy*(x+dx)+y+dy]*b_buffer[sy*(x+dx)+y+dy];
                        }
                    }
                }
                numerator += tmp1;
                denominator += tmp2;
            }
        }
    }

    if (denominator < 0.00000001){
        m_epsilon = 0.;
    }
    else{
        m_epsilon = - numerator/denominator;
    }

    std::cout << "epsilon empirical = " << m_epsilon << std::endl;
}

void Sure::run_local(){
    calculateEpsilonMap();

    m_epsilon = 0.0;
    int n = 0;

    int w = m_a->getSizeX();
    int h = m_a->getSizeY();
    float* ref_buffer = m_ref->getBuffer();
    float* eps_buffer = m_epsilonMap->getBuffer();
    for (int x = 0 ; x < w ; x++){
        for (int y = 0 ; y < h ; y++){
            if (ref_buffer[h*x+y] >= m_threshold){
                n++;
                m_epsilon += eps_buffer[h*x+y];
            }
        }
    }
    m_epsilon /= n;

//    GaussianFiltering(IN2D_eps,m_windowSize,G_IN2D_eps,w,h);
//    AiryScan3DStat(G_IN2D_eps, mean_eps, mode_eps, w, h);
//    m_epsilon = mode_eps;

    std::cout << "epsilon local = " << m_epsilon << std::endl;
}

SImageFloat* Sure::calculateEpsilonMap(){
    unsigned int sx = m_a->getSizeX();
    unsigned int sy = m_a->getSizeY();
    float* a_buffer = m_a->getBuffer();
    float* b_buffer = m_b->getBuffer();
    float* ref_buffer = m_ref->getBuffer();

    // calculate epsilon map
    m_epsilonMap = new SImageFloat(sx, sy, 1, 1, 1);
    m_epsilonMap->allocate();
    float* m_eps_buffer = m_epsilonMap->getBuffer();
    float tmp1, tmp2;
    for (int x = 0 ; x < sx ; x++){
        for (int y = 0 ; y < sy ; y++){
            //int np = 0;
            tmp1 = 0.;
            tmp2 = 0.;

            for (int dx=-m_windowSize; dx<=m_windowSize; dx++){
                for (int dy=-m_windowSize; dy<=m_windowSize; dy++){
                    if (INCLUS(x+dx,y+dy,sx,sy)!=0){
                        //np += 1;
                        tmp1 += (ref_buffer[sy*(x+dx)+y+dy] - a_buffer[sy*(x+dx)+y+dy])*b_buffer[sy*(x+dx)+y+dy];
                        tmp2 += b_buffer[sy*(x+dx)+y+dy]*b_buffer[sy*(x+dx)+y+dy];
                    }
                }
            }

            if (tmp2 < 0.00000001){
                m_eps_buffer[sy*x+y] = 0;
            }
            else{
                m_eps_buffer[sy*x+y] = - tmp1/tmp2;
            }
        }
    }
    return m_epsilonMap;
}

void Sure::AiryScan3DStat(float *IN_i, float &mean, float &mode, int w, int h)

{
    float vmax = 0.;
    float vmin = 1.e6;
    mean = 0.;

    for (int i=0; i<w*h; i++){
        mean += IN_i[i];
        if (IN_i[i] > vmax) vmax = IN_i[i];
        if (IN_i[i] < vmin) vmin = IN_i[i];
    }
    mean /= (w*h);

    float H[100];
    for (int ind=0; ind<100; ind++) H[ind] = 0.;
    float Delta = (vmax - vmin)/100.;

    for (int ind=0; ind<w*h; ind++){
        int num = MIN(99, (int)((IN_i[ind] - vmin)/Delta));
        H[num] += 1;
    }

    int Hmax = 0, Indmax;
    for (int ind=0; ind<100; ind++){
        if  (H[ind] > Hmax){
            Hmax = H[ind];
            Indmax = ind;
        }
    }

    mode = Indmax*Delta+vmin;
}

SImageFloat* Sure::epsilonMap(){
    return m_epsilonMap;
}

float Sure::epsilon(){
    return m_epsilon;
}
