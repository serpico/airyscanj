/// \file StackSure.cpp
/// \brief StackSure class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2020

#include <iostream>

#include "StackSure.h"
#include "Sure.h"

#include <simageio>

StackSure::StackSure(SImageFloat* stack, unsigned int ref_idx, unsigned int m1, unsigned int m2, unsigned int n1, unsigned int n2){
    m_stack = stack;
    m_ref_idx = ref_idx;
    m_m1 = m1;
    m_m2 = m2;
    m_n1 = n1;
    m_n2 = n2;
    m_windowSize = 11;
}

void StackSure::setWindowSize(int value){
    m_windowSize = value;
}

void StackSure::run(){

    int sx = m_stack->getSizeX();
    int sy = m_stack->getSizeY();
    int sz = m_stack->getSizeZ();
    float* stack_buffer = m_stack->getBuffer();
    SImageFloat* a = new SImageFloat(sx, sy);
    a->allocate();
    float* a_buffer = a->getBuffer();
    SImageFloat* b = new SImageFloat(sx, sy);
    b->allocate();
    float* b_buffer = b->getBuffer();

    for (int x = 0 ; x < sx ; x++){
        for (int y = 0 ; y < sy ; y++){
            a_buffer[sy*x+y] = 0;
            for (int d = m_m1 ; d <= m_m2 ; d++){
                a_buffer[sy*x+y] += stack_buffer[sz*(sy*x+y)+d];
            }
            b_buffer[sy*x+y] = 0;
            for (int d = m_n1 ; d <= m_n2 ; d++){
                b_buffer[sy*x+y] += stack_buffer[sz*(sy*x+y)+d];
            }
        }
    }

    Sure process;
    process.setRef(m_stack->getSlice(m_ref_idx));
    process.setA(a);
    process.setB(b);
    process.setWindowSize(m_windowSize);
    m_epsilonMap = process.calculateEpsilonMap();
}

SImageFloat* StackSure::getEpsilonMap(){
    return m_epsilonMap;
}
