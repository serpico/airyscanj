/*=============================================================================

_Header

_Module_Name 	: MAD.cc

_Directory	: 

_Description 	: Description de MAD.cc

_Calls   	:

_Authors	: Charles KERVRANN

_Contents       :

_Last Release   : 17/11/03

_end

=============================================================================*/






/*===========================================================================*/

#include "define.h"
#include "include.h"

/*===========================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : QSort
_Subject  :

-----------------------------------------------------------------------------*/

void QSort(double *a,int left,int right)
{
  int i,j;
  double temp;
  
  i    = left;
  j    = right;
  temp = a[left];
  
  if(left>right) return;
  
  while(i!=j){
    while(a[j]>=temp && j>i)  j--;
    if(j>i)                   a[i++]=a[j];
    
    while(a[i]<=temp && j>i) i++;
    
    if(j>i) a[j--]=a[i];
  }
  
  a[i] = temp;
  QSort(a, left, i-1);
  QSort(a, i+1, right);
}

/*===========================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : compare
_Subject  : Compare two integers
-----------------------------------------------------------------------------*/

int compare(const void *i, const void *j)
{
  float a=*((float *)i);
  float b=*((float *)j);

  if (a > b)
    return (1);
  else
    if (a < b)
      return (-1);
    else
      return (0);
}

/*===========================================================================*/







/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : Tri
_Subject  :
-----------------------------------------------------------------------------*/

void Tri(float *tab, int N)
{
 qsort((float *) tab, N, sizeof(float), compare);
}

/*===========================================================================*/




/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : WienerFiltering
_Subject  :
-----------------------------------------------------------------------------*/

void WienerFiltering(float *I, float eta, float sigma, int patchsize, float *Iwiener, int w, int h)
{
  int ind, i, j, u, v;
  int du = patchsize;
  int dv = patchsize;

  printf("Patch size (default: 2): %d\n", patchsize);
  printf("Denoising parameter (default: 1.25): %2.2f\n", eta);
  float *npix  = new float[w*h];
  float *Imean = new float[w*h];
  float *Ivar  = new float[w*h];  

  for (i=0; i<w*h; i++){
    npix[i]     = 0.;
    Imean[i]    = 0.;
    Ivar[i]     = 0.;
  }

  for(i=0; i<w; i++)
    for(j=0; j<h; j++){
      ind=i+w*j;
      for (int u=-du; u<=du; u++)
	for (int v=-dv; v<=dv; v++){
	  if (INCLUS(i+u,j+v,w,h)!=0){
	    Imean[ind] += I[i+u+w*(j+v)];
	    npix[ind]+=1.; 
	  }
	}
      Imean[ind] /= npix[ind]; 
    }
 
  for(i=0; i<w; i++)
    for(j=0; j<h; j++){ 
      ind=i+w*j;
      for (int u=-du; u<=du; u++)
	for (int v=-dv; v<=dv; v++)
	  if (INCLUS(i+u,j+v,w,h)!=0) Ivar[ind] += (I[i+u+w*(j+v)]-Imean[ind])*(I[i+u+w*(j+v)]-Imean[ind]);
      Ivar[ind] /= npix[ind];   
      Ivar[ind] = MAX(Ivar[ind], 1e-5);
    }

 for(ind=0; ind<w*h; ind++){
   float beta = MAX(0, (Ivar[ind]-eta*sigma*eta*sigma)/Ivar[ind]);
   Iwiener[ind] = Imean[ind] + beta*(I[ind]-Imean[ind]);
 }
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : Median3H
_Subject  :
-----------------------------------------------------------------------------*/

void Median3H(float *I, float *Imed, int w, int h)
{
  int   ind, i, j;
  float *Tab = new float[3];

  for(ind=0; ind<w*h; ind++) Imed[ind] = I[ind];

  for(i=1; i<w-1; i++)
    for(j=1; j<h-1; j++){
      ind    = i+w*j;	
      Tab[0] = I[ind];
      Tab[1] = I[ind+1];
      Tab[2] = I[ind-1];
      Tri(Tab,3);
      Imed[ind] = Tab[1];
    }

  delete[] Tab;
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : Median3V
_Subject  :
-----------------------------------------------------------------------------*/

void Median3V(float *I, float *Imed, int w, int h)
{
  int   ind, i, j;
  float *Tab  = new float[3];

  for(ind=0; ind<w*h; ind++) Imed[ind] = I[ind];

  for(i=1; i<w-1; i++)
    for(j=1; j<h-1; j++){
      ind    = i+w*j;	
      Tab[0] = I[ind];
      Tab[1] = I[ind+w];
      Tab[2] = I[ind-w];
      Tri(Tab,3);
      Imed[ind] = Tab[1];
    }
  
  delete[] Tab;
}

/*===========================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : Median3D45
_Subject  :
-----------------------------------------------------------------------------*/

void Median3D45(float *I, float *Imed, int w, int h)
{
  int   ind, i, j;
  float *Tab  = new float[3];

  for(ind=0; ind<w*h; ind++) Imed[ind] = I[ind];

  for(i=1; i<w-1; i++)
    for(j=1; j<h-1; j++){
      ind    = i+w*j;	
      Tab[0] = I[ind];
      Tab[1] = I[ind+w-1];
      Tab[2] = I[ind-w+1];
      Tri(Tab,3);
      Imed[ind] = Tab[1];
    }
  
  delete[] Tab;
}

/*===========================================================================*/








/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : Median3D135
_Subject  :
-----------------------------------------------------------------------------*/

void Median3D135(float *I, float *Imed, int w, int h)
{
  int   ind, i, j;
  float *Tab  = new float[3];

  for(ind=0; ind<w*h; ind++) Imed[ind] = I[ind];

  for(i=1; i<w-1; i++)
    for(j=1; j<h-1; j++){
      ind    = i+w*j;	
      Tab[0] = I[ind];
      Tab[1] = I[ind+w+1];
      Tab[2] = I[ind-w-1];
      Tri(Tab,3);
      Imed[ind] = Tab[1];
    }
  
  delete[] Tab;
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : Median5HV
_Subject  :
-----------------------------------------------------------------------------*/

void Median5HV(float *I, float *Imed, int w, int h)
{
  int   ind, i, j;
  float *Tab = new float[5];

  for(ind=0; ind<w*h; ind++) Imed[ind] = I[ind];

  for(i=1; i<w-1; i++)
    for(j=1; j<h-1; j++){
      ind    = i+w*j;	
      Tab[0] = I[ind];
      Tab[1] = I[ind+1];
      Tab[2] = I[ind-1];
      Tab[3] = I[ind+w];
      Tab[4] = I[ind-w];
      Tri(Tab,5);
      Imed[ind] = Tab[2];
    }
  
  delete[] Tab;
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : Median5D
_Subject  :
-----------------------------------------------------------------------------*/

void Median5D(float *I, float *Imed, int w, int h)
{
  int   ind, i, j;
  float *Tab = new float[5];

  for(ind=0; ind<w*h; ind++) Imed[ind] = I[ind];

  for(i=1; i<w-1; i++)
    for(j=1; j<h-1; j++){
      ind    = i+w*j;	
      Tab[0] = I[ind];
      Tab[1] = I[ind+w+1];
      Tab[2] = I[ind+w-1];
      Tab[3] = I[ind-w-1];
      Tab[4] = I[ind-w+1];
      Tri(Tab,5);
      Imed[ind] = Tab[2];
    }
  
  delete[] Tab;
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : Median9
_Subject  :
-----------------------------------------------------------------------------*/

void Median9(float *I, float *Imed, int w, int h)
{
  int   ind, i, j;
  float *Tab = new float[9];

  for(ind=0; ind<w*h; ind++) Imed[ind] = I[ind];

  for(i=1; i<w-1; i++)
    for(j=1; j<h-1; j++){
      ind    = i+w*j;	
      Tab[0] = I[ind];
      Tab[1] = I[ind+1];
      Tab[2] = I[ind-1];
      Tab[3] = I[ind+w];
      Tab[4] = I[ind-w];
      Tab[5] = I[ind+1+w];
      Tab[6] = I[ind-1-w];
      Tab[7] = I[ind+w-1];
      Tab[8] = I[ind-w+1];
      Tri(Tab,9);
      Imed[ind] = Tab[4];
    }

  delete[] Tab;
}

/*===========================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : Peak9
_Subject  :
-----------------------------------------------------------------------------*/

void Peak9(float *I, float *var, float th, float *Ipeak, int w, int h)
{
  int   ind, i, j;
  float *Ibuffer = new float[w*h];
  float *Tab     = new float[9];

  for(ind=0; ind<w*h; ind++){
    Ibuffer[ind] = I[ind];
    Ipeak[ind]   = I[ind];
  }

  for(i=1; i<w-1; i++)
    for(j=1; j<h-1; j++){
      ind    = i+w*j;	
      Tab[0] = Ibuffer[ind];
      Tab[1] = Ibuffer[ind+1];
      Tab[2] = Ibuffer[ind-1];
      Tab[3] = Ibuffer[ind+w];
      Tab[4] = Ibuffer[ind-w];
      Tab[5] = Ibuffer[ind+1+w];
      Tab[6] = Ibuffer[ind-1-w];
      Tab[7] = Ibuffer[ind+w-1];
      Tab[8] = Ibuffer[ind-w+1];
      
      Tri(Tab,9);
      Ibuffer[ind] = Tab[4];
    }

    for(i=1; i<w-1; i++)
    for(j=1; j<h-1; j++){
      ind    = i+w*j;
      float val = (I[ind]+I[ind+1]+I[ind-1]+I[ind+w]+I[ind-w]+I[ind+1+w]+I[ind+1-w]+I[ind-1+w]+I[ind-1-w])/9;
      //if (val<th*sqrt(var[ind])) 
      if (fabs(I[ind]-Ibuffer[ind])>th*sqrt(var[ind]))	
      Ipeak[ind] = Ibuffer[ind];
    }

  delete[] Ibuffer;
  delete[] Tab;
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : Median13
_Subject  :
-----------------------------------------------------------------------------*/

void Median13(float *I, float *Imed, int w, int h)
{
  int   ind, i, j;
  float *Tab  = new float[13];

  for(ind=0; ind<w*h; ind++) Imed[ind] = I[ind];

  for(i=1; i<w-2; i++)
    for(j=1; j<h-2; j++){
      ind    = i+w*j;	
      Tab[0] = I[ind];
      Tab[1] = I[ind+1];
      Tab[2] = I[ind-1];
      Tab[3] = I[ind+w];
      Tab[4] = I[ind-w];
      Tab[5] = I[ind+1+w];
      Tab[6] = I[ind-1-w];
      Tab[7] = I[ind+w-1];
      Tab[8] = I[ind-w+1];
      Tab[9] = I[ind+2];
      Tab[10] = I[ind-2];
      Tab[11] = I[ind+2*w];
      Tab[12] = I[ind-2*w];
      Tri(Tab,13);
      Imed[ind] = Tab[6];
    }

  delete[] Tab;
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : EM_M2G
_Subject  : 
-----------------------------------------------------------------------------*/

void  EM_2MG(float *tab, int N, float *MU0, float *VAR0, float *MU1, float *VAR1, 
	     float *pi0)
{
  int    i;
  float  PI0;
  float  PI1;
  double f0, f1;
  double Z0, Z1;
  float  *t0, sum_t0;
  float  *t1, sum_t1;
  float mu0, var0, mu1, var1;
  
  // Initialization
  // --------------
  t0   = new float [N];
  t1   = new float [N];
  mu0  = *MU0;
  mu1  = *MU1;
  var0 = *VAR0;
  var1 = *VAR1;
  PI0  = *pi0;
  PI1  = 1-PI0;



  // EM procdure
  // -----------
  for (int iter=0; iter<10; iter++){  
    Z0 = 0.;
    Z1 = 0.;
    for (i=0; i<N; i++){
      Z0 += exp(-(tab[i] - mu0)*(tab[i] - mu0)/(2*var0));
      Z1 += exp(-(tab[i] - mu1)*(tab[i] - mu1)/(2*var1));
    }
  
    for (i=0; i<N; i++){
      f0 = exp(-(tab[i] - mu0)*(tab[i] - mu0)/(2*var0))/Z0;
      f1 = exp(-(tab[i] - mu1)*(tab[i] - mu1)/(2*var1))/Z1;
	
      t0[i] = PI0*f0/(PI0*f0+PI1*f1);
      t1[i] = PI1*f1/(PI0*f0+PI1*f1);
    }

    PI0 = 0.;
    PI1 = 0.;
    for (i=0; i<N; i++){
      PI0 += t0[i];
      PI1 += t1[i];
    }
    PI0 /= N;
    PI1 /= N;
  

    mu0    = 0.;
    sum_t0 = 0.;
    for (i=0; i<N; i++){
      mu0 += t0[i]*tab[i];
      sum_t0 += t0[i];
    }

    mu0 /= sum_t0;

    var0 = 0.;
    for (i=0; i<N; i++)
      var0 += t0[i]*(tab[i]-mu0)*(tab[i]-mu0);
    var0 /= sum_t0;
    
    mu1    = 0.;
    sum_t1 = 0.;
    for (i=0; i<N; i++){
      mu1 += t1[i]*tab[i];
      sum_t1 += t1[i];
    }

    mu1 /= sum_t1;

    var1 = 0.;
    for (i=0; i<N; i++)
      var1 += t1[i]*(tab[i]-mu1)*(tab[i]-mu1);
    var1 /= sum_t1;

    //printf("\nEM -> mu0 = %2.2f std0 = %2.2f PI0 = %2.2f Z0= %f\n", mu0, sqrt(var0), PI0, Z0);
    //printf("EM -> mu1 = %2.2f std1 = %2.2f PI1 = %2.2f Z1 = %f\n\n", mu1, sqrt(var1), PI1, Z1);


  }

  Z0 = 0.;
  Z1 = 0.;
  for (i=0; i<N; i++){
    Z0 += exp(-(tab[i] - mu0)*(tab[i] - mu0)/(2*var0));
    Z1 += exp(-(tab[i] - mu1)*(tab[i] - mu1)/(2*var1));
  }


  *MU0  = mu0;
  *MU1  = mu1;
  *VAR0 = var0;
  *VAR1 = var1;
  *pi0  = PI0;

}

/*==============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : MeanLTS
_Subject  : Compute the robust standard deviation of the data defined by
            Rousseeuw and Leroy on the complete image support (Least Trimmed
            of Squares)
-----------------------------------------------------------------------------*/

float MeanLTS(float *tab, int N)
{
  int    i, x, Nmed;
  float  Vlts  = 0.;
  float  Vmean = 0.;
  float  *I, *T;


  /* Initialization */
  /* -------------- */
  I = new float [N];
  T = new float [25500];

  for (i=0; i<N; i++)
    Vmean += tab[i];
  Vmean /= N;

  for (i=0; i<N; i++){
    x    = (int)(100*fabs(tab[i]-Vmean));
    T[x] = tab[i];
    I[i] = (float)(x);
  }

  /* Computation of Med_i(tab) */
  /* ------------------------- */
  qsort((float *) I, N, sizeof(float), compare);

  /* Least Trimmed Squares */
  /* --------------------- */
  Nmed  = (int)(0.5*N);

  for (i=0; i<Nmed; i++){
    //printf(" %f", I[i]);
    x = (int)(I[i]);
    Vlts += T[x];
  }
  //printf("\n");
  Vlts /= Nmed;

  delete[] I;
  delete[] T;

  return(Vlts);
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : SigmaLTS
_Subject  : Compute the robust standard deviation of the data defined by
            Rousseeuw and Leroy on the complete image support (Least Trimmed
            of Squares)
-----------------------------------------------------------------------------*/

float SigmaLTS(float *tab, int N)
{
  int    i;
  int    k=0;
  int    Nmed;
  float  sigma;
  float  *I;


  /* Initialization */
  /* -------------- */
  I = new float [N];

  for (i=0; i<N; i++)
    //if (tab[i]!=0.)
    {
      I[k]=(tab[i]*tab[i]);
      k++;
    }
  N = k-1;

  if (N<=0) 
    sigma = 0.005;
  else {
    /* Computation of Med_i(tab) */
    /* ------------------------- */
    qsort((float *) I, N, sizeof(float), compare);

    /* Least Median of Squares estimation of sigma */
    /* ------------------------------------------- */
    Nmed  = (int)(0.5*N);
    sigma = 0.;
    for (i=0; i<Nmed; i++) sigma += I[i];
    sigma = MAX(0.005, 2.6477*sqrt(sigma/Nmed));
  }

  delete[] I;

  return(sigma);
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : SigmaLMedS
_Subject  : Compute the robust standard deviation of the data defined by
            Rousseeuw and Leroy on the complete image support (Least Median
            of Squares)
-----------------------------------------------------------------------------*/

float SigmaLMedS(float *tab, int N)
{
  int    i;
  int    k=0;
  int    Nmed;
  float  sigma;
  float  med_tab;
  float  *I;


  /* Initialization */
  /* -------------- */
  for (i=0; i<N; i++)
    if (tab[i]!=0){
      tab[k]=tab[i];
      k++;
    }
  N = k-1;


  if (N<=0)
    sigma = 0.005;
  else
  {
    I = new float [N];
    for (i=0; i<N; i++)
      I[i] = tab[i];


    /* Computation of Med_i(tab) */
    /* ------------------------- */
    Nmed = (int)(0.5*N);
    qsort((float *) tab, N, sizeof(float), compare);
    med_tab = tab[Nmed];


    /* Least Median of Squares estimation of sigma */
    /* ------------------------------------------- */
    for (i=0; i<N; i++){
      I[i]   -= med_tab;
      tab[i]  = ABSX(I[i]);
    }

    qsort((float *) tab, N, sizeof(float), compare);
    sigma = MAX(0.005, 1.4826*tab[Nmed]);

    delete[] I;
  }

  return(sigma);
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : SigmaLMS
_Subject  : Compute the standard deviation of the data (Least Mean of Squares)
-----------------------------------------------------------------------------*/

float SigmaLMS(float *tab, int N)
{
  int    i;
  int    k=0;
  float  sigma2=0.;

  for (i=0; i<N; i++)
    if (tab[i]!=0){
      tab[k]=tab[i];
      k++;
    }
  N = k-1;

  for (i=0; i<N; i++)
    sigma2 += tab[i]*tab[i];

  if (N<=0)
    return(0.005);
  else
    return(sqrt(sigma2/N));
}

/*===========================================================================*/







/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : PseudoResidu
_Subject  :

-----------------------------------------------------------------------------*/

void  PseudoResidu(float *pixm, float *residu, int w, int h)
{
  int i, j;
  int ind;

  for(i=0; i<w-1; i++)
    for(j=0; j<h-1; j++){
      ind=i+w*j;
      residu[ind]=(2*pixm[ind]-pixm[ind+1]-pixm[ind+w])/sqrt(6);
    }

  
  i=w-1;
  for(j=0; j<h-1; j++){
    ind=i+w*j;
    residu[ind]=(2*(pixm[ind])-pixm[ind-1]-pixm[ind+w])/sqrt(6);
  }

  j=h-1;
  for(i=0; i<w-1; i++){
    ind=i+w*j;
    residu[ind]=(2*(pixm[ind])-pixm[ind+1]-pixm[ind-w])/sqrt(6);
  }

  i=w-1;j=h-1;
  ind=i+w*j;
  residu[ind]=(2*(pixm[ind])-pixm[ind-1]-pixm[ind-w])/sqrt(6);
  

}

/*=============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : ResiduML
_Subject  :

-----------------------------------------------------------------------------*/

void  ResiduML(float *pixm, float *residu, int w, int h)
{
  int   ind;
  int   k=0;
  float m=0.;

  for (ind=0; ind<w*h; ind++)
    if (pixm[ind]!=0){
      m += pixm[ind];
      k++;
    }

  for (ind=0; ind<w*h; ind++)
    if (pixm[ind]!=0)
      residu[ind]=pixm[ind]-m/k;
    else
      residu[ind]=0.;

}

/*=============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : carte_MAD_gaussien
_Subject  :

-----------------------------------------------------------------------------*/

void carte_MAD_gaussien(float *p_pixmap, float sig, float *sig_I, int p, int w, int h)
{
  int i, j, i_, j_, n, m;
  int card;
  int sampler=1;
  float sigl;

  //p=7;
  printf("   Local variance estimation over %d x %d patches\n\n", 2*p+1, 2*p+1);
  //if (p>7) sampler=(int)(2*SAMPLER);
  //else     sampler=1;
  //p = MIN(p,5);

  float *residuL = new float[(2*p+1)*(2*p+1)];
  float *residuG = new float[w*h];

  PseudoResidu(p_pixmap,residuG,w,h);

  for(i=0; i<w; i++){
    for(j=0; j<h; j++){
      card = 0;

      for(n=-p; n<=p; n++)
	for(m=-p; m<=p; m++){
	  i_=i+n;
	  j_=j+m;
	  if (INCLUS(i_,j_,w,h)!=0){
	    if (((n+1)%sampler==0) && ((m+1)%sampler==0)){
	      residuL[card]=residuG[i_+w*j_];
	      card++;
	    }
	  }
	}

      //sigl=SigmaLMedS(residuL,card);
      sigl=SigmaLTS(residuL,card);
      //sigl=SigmaLMS(residuL,card);
      //sig_I[i+w*j]=MAX(sig,sigl*sigl);
      //printf("%2.2f %2.2f |", sig_I[i+w*j], sig);
      sig_I[i+w*j]=sigl*sigl;
    }
  }

  delete[] residuG;
  delete[] residuL;
}

/*==============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : carte_MAD_gaussien_
_Subject  :

-----------------------------------------------------------------------------*/

void carte_MAD_gaussien_(float *p_pixmap, float sig, float *sig_I, int p, int w, int h)
{
  int i, j, i_, j_, n, m;
  int card;
  int sampler=1;
  float sigl;

  //if (p>7) sampler=(int)(2*SAMPLER);
  //else     sampler=1;
  //p = MIN(p,5);
  
  float *residuL = new float[(2*p+1)*(2*p+1)];
  float *residuG = new float[w*h];

  PseudoResidu(p_pixmap,residuG,w,h);

  for(i=0; i<w; i++){
    for(j=0; j<h; j++){
      card = 0;

      for(n=-p; n<=p; n++)
	for(m=-p; m<=p; m++){
	  i_=i+n;
	  j_=j+m;
	  if (INCLUS(i_,j_,w,h)!=0){
	    if (((n+1)%sampler==0) && ((m+1)%sampler==0)){
	      residuL[card]=residuG[i_+w*j_];
	      card++;
	    }
	  }
	}

      sigl=SigmaLMS(residuL,card);
      sig_I[i+w*j]=MIN(sig,sigl*sigl)+EPS_0;
    }
  }


  delete[] residuG;
  delete[] residuL;
}

/*==============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : carte_MAD_speckle
_Subject  :

-----------------------------------------------------------------------------*/

void carte_MAD_speckle(float *p_pixmap, float sig, float *sig_I, int p, int w, int h)
{
  int i, j, i_, j_, n, m;
  int card,  ind;
  int sampler=1;
  float sigl, moyl;

  //if (p>7) sampler=(int)(2*SAMPLER);
  //else     sampler=1;
  p = MIN(p,5);

  float *residuL = new float[(2*p+1)*(2*p+1)];

  for(i=0; i<w; i++)
    for(j=0; j<h; j++){
      card = 0;
      moyl=0.;
      for(n=-p; n<=p; n++)
	for(m=-p; m<=p; m++){
	  i_=i+n;
	  j_=j+m;
	  if (INCLUS(i_,j_,w,h)!=0){
	    if (((n+1)%sampler==0) && ((m+1)%sampler==0)){
	      residuL[card]=p_pixmap[i_+w*j_];
	      moyl+=p_pixmap[i_+w*j_];
	      card++;
	    }
	  }
	}

      for (ind=0; ind<card; ind++)
	residuL[ind]-=moyl/card;

      //sigl=SigmaLMedS(residuL,card);
      sigl=SigmaLTS(residuL,card);
      sig_I[i+w*j]=MAX(sig,sigl*sigl);
    }

  delete[] residuL;
}

/*==============================================================================*/







/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : carte_eta
_Subject  :

-----------------------------------------------------------------------------*/

void carte_eta(float *p_pixmap, int w, int h, float sig, int p, int N, float *c_eta)
{
  int i, j, i_, j_, n, m;
  float card;
  float npts;
  float etaM=0.;
  float etam=1e3;
  
  for(i=0; i<w; i++){
    for(j=0; j<h; j++){
      card = 0;
      npts=0.;

      for(n=-p; n<=p; n++)
	for(m=-p; m<=p; m++){
	  i_=i+n;
	  j_=j+m;
	  if (INCLUS(i_,j_,w,h)!=0){
	    npts++;
	    if (p_pixmap[i_+w*j_]<sig)
	      card++;
	  }
	}

      c_eta[i+w*j] = sqrt(2*log((N*(N-1))/(1-card/npts)));
      etam = MIN( c_eta[i+w*j],etam);
      etaM = MAX( c_eta[i+w*j],etaM);
    }
  }

  printf("\neta min = %f\n", etam);
  printf("eta max = %f\n\n", etaM);

}

/*==============================================================================*/









/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : noise_linear_model
_Subject  :

-----------------------------------------------------------------------------*/

void noise_linear_model(float *residu, float *pixmap, float *sig_eps, float *sig_eta, int p, int w, int h)
{
  int i, j, i_, j_, n, m;
  float card;

  float *zi = new float[w*h];
  float *ui = new float[w*h];

  float A[2][2];
  float A_1[2][2];
  float det;
  float B[2];


  for(i=0; i<w; i++)
    for(j=0; j<h; j++){
      card = 0;

      for(n=-p; n<=p; n++)
	for(m=-p; m<=p; m++){
	  i_=i+n;
	  j_=j+m;
	  if (INCLUS(i_,j_,w,h)!=0){
	    zi[i+w*j]+=residu[i_+w*j_]*residu[i_+w*j_];
	    ui[i+w*j]+=pixmap[i_+w*j_];
	    card++;
	  }
	}
      zi[i+w*j] /= card;
      
      ui[i+w*j] /= card;
    }
  
  A[0][0]=0.;
  A[0][1]=0.;
  A[1][0]=0.;
  A[1][1]=0.;
  B[0]=0.;
  B[1]=0.;

  for(i=0; i<w*h; i++){
    A[0][0] += ui[i]*ui[i];
    A[0][1] += ui[i];
    B[0]    += ui[i]*zi[i];
    B[1]    += zi[i];
  }
  A[1][0] = A[0][1];
  A[1][1] = (float)(w*h);

  det = A[0][0]*A[1][1] - A[1][0]*A[0][1];
  printf("%f ", det); 
  
  
  A_1[0][0] = A[1][1]/det;
  A_1[0][1] = -A[0][1]/det;
  A_1[1][0] = -A[1][0]/det;
  A_1[1][1] = A[0][0]/det;

  *sig_eps = A_1[0][0]*B[0]+A_1[0][1]*B[1];
  *sig_eta = A_1[1][0]*B[0]+A_1[1][1]*B[1];

  printf(" sig_eps=%f,   sig_eta = %f\n",   *sig_eps, *sig_eta);
  
  delete[] zi;
  delete[] ui;

  // a revoir en moindres carres avec une contrainte de positivite
}

/*==============================================================================*/







/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : carte_corr
_Subject  :

-----------------------------------------------------------------------------*/

void carte_corr(float *ima_u, float *ima_v, int p, int w, int h, float *corr)
{
  int i, j, i_, j_, n, m;
  float card;
  float mean_u, mean_v;
  float var_u, var_v;
  float cov;
  

  for(i=0; i<w; i++)
    for(j=0; j<h; j++){
      card   = 0.;
      mean_u = 0.;
      mean_v = 0.;


     for(n=-p; n<=p; n++)
	for(m=-p; m<=p; m++){
	  i_=i+n;
	  j_=j+m;
	  if (INCLUS(i_,j_,w,h)!=0){
	    mean_u += ima_u[i_+w*j_];
	    mean_v += ima_v[i_+w*j_];
	    card   += 1.;
	  }
	}
     mean_u /= card;
     mean_v /= card;
     //printf(" %f  %f \n", mean_u, mean_v);
     
     var_u = 0.;
     var_v = 0.;
     cov  = 0.;

     for(n=-p; n<=p; n++)
	for(m=-p; m<=p; m++){
	  i_=i+n;
	  j_=j+m;
	  if (INCLUS(i_,j_,w,h)!=0){
	    var_u += (ima_u[i_+w*j_]-mean_u)*(ima_u[i_+w*j_]-mean_u);
	    var_v += (ima_v[i_+w*j_]-mean_v)*(ima_v[i_+w*j_]-mean_v);
	    cov   += (ima_u[i_+w*j_]-mean_u)*(ima_v[i_+w*j_]-mean_v);
	  }
	}

     //printf(" %f %f %f \n", var_u, var_v, cov); 
     var_u /= card;
     var_v /= card;
     cov   /= card;

     corr[i+w*j] = fabs(cov)/(sqrt(var_u)*sqrt(var_v)+EPS_0);
     
     corr[i+w*j] = corr[i+w*j] * sqrt(((2*p+1)*(2*p+1)-2) / (1.-corr[i+w*j]*corr[i+w*j]));
     
     //corr[i+w*j] = fabs(sqrt(var_v));//fabs(cov)/(sqrt(var_u*var_v)+EPS_0);
     //printf(" %f %f %f\n", sqrt(var_v), corr[i+w*j], card);
    }
  
}

/*==============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : carte_snr
_Subject  :

-----------------------------------------------------------------------------*/

void carte_snr(float *ima_v, float sigma, int p, int w, int h, float *snr)
{
  int i, j, i_, j_, n, m;
  float card;
  float mean_v;
  float var_v;
  float cov;
  
  double *Var_v = new double[w*h];
  double *Cov   = new double[w*h];
  double sum_I =0.;
  double sum_N=0.;

  for(i=0; i<w; i++)
    for(j=0; j<h; j++){
      card   = 0.;
      mean_v = 0.;
      
      for(n=-p; n<=p; n++)
	for(m=-p; m<=p; m++){
	  i_=i+n;
	  j_=j+m;
	  if (INCLUS(i_,j_,w,h)!=0){
	    mean_v += ima_v[i_+w*j_];
	    card   += 1.;

	  }
	}
     mean_v /= card;
   
     var_v = 0.;
     cov   = 0.;
 
     for(n=-p; n<=p; n++)
	for(m=-p; m<=p; m++){
	  i_=i+n;
	  j_=j+m;
	  if (INCLUS(i_,j_,w,h)!=0){
	    var_v += (ima_v[i_+w*j_]-mean_v)*(ima_v[i_+w*j_]-mean_v);

	    double a = (float)(rand())/(float)(RAND_MAX);
	    double b = (float)(rand())/(float)(RAND_MAX);
	    double I = sigma*sqrt(-2*log((double)(1e-10 + (1-2e-10)*a)))*cos((double)(2*PI*b));
	    
	    cov   += (I)*(ima_v[i_+w*j_]-mean_v);
	    sum_I += I*I;
	    sum_N += 1.;
	  }
	}

     var_v /= card;
     cov   /= card;

     Cov[i+w*j]   = cov;
     Var_v[i+w*j] = var_v;
    }

  sum_I/=sum_N;
  //printf("\nvarI=%f N=%f\n", sqrt(sum_I), sum_N);
 
  
 for(i=0; i<w-1; i++)
    for(j=0; j<h-1; j++){
      double dcov_x   =  Cov[i+w*j] - Cov[i+1+w*j];
      double dcov_y   =  Cov[i+w*j] - Cov[i+w*(j+1)];
      double dvar_v_x =  Var_v[i+w*j] - Var_v[i+1+w*j]+EPS_0;
      double dvar_v_y =  Var_v[i+w*j] - Var_v[i+w*(j+1)]+EPS_0;
      //snr[i+w*j] = fabs((dcov_x/dvar_v_x + dcov_y/dvar_v_y) -1);
      snr[i+w*j] = fabs(Cov[i+w*j]/sqrt(Var_v[i+w*j]));
      //printf("dcov_x=%f dcov_y=%f dvar_x=%f dvar_y=%f snr=%f  \n", dcov_x, dcov_y, dvar_v_x, dvar_v_y, snr[i+j*w]);
    }


  
}

/*==============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : QR_metrics
_Subject  :

-----------------------------------------------------------------------------*/

void QR_metrics(float *pixmap, int w, int h, int p, float *Q, float *R)
{

  int i, j, k;
  double lambda1, lambda2;
  double j11, j22, j12;



  /* Memory allocation */
  /* ----------------- */
  double *Ix      = new double[w*h]; 
  double *Iy      = new double[w*h]; 
  double *Ix2     = new double[w*h]; 
  double *Iy2     = new double[w*h]; 
  double *IxIy    = new double[w*h]; 
  double *J_Ix2   = new double[w*h]; 
  double *J_Iy2   = new double[w*h]; 
  double *J_IxIy  = new double[w*h]; 
 


  /* Compute the image gradients */
  /* --------------------------- */
  for(i=1; i<w-1; i++)
    for(j=1; j<h-1; j++){
      int r=i+w*j;
      Ix[r] = (10*(pixmap[r-1]-pixmap[r+1])+3*(pixmap[r-1-w]-pixmap[r+1-w])+3*(pixmap[r-1+w]-pixmap[r+1+w]))/4.;
      Iy[r] = (10*(pixmap[r-w]-pixmap[r+w])+3*(pixmap[r-w-1]-pixmap[r+w-1])+3*(pixmap[r-w+1]-pixmap[r+w+1]))/4.;
      //Ix[r] = pixmap[r]-pixmap[r+1];
      //Iy[r] = pixmap[r]-pixmap[r+w];
    }
  

  /* Compute the components of structure tensor */
  /* ------------------------------------------ */
  for (k=0; k<w*h; k++){
    Ix2[k]  = Ix[k]*Ix[k];
    Iy2[k]  = Iy[k]*Iy[k];
    IxIy[k] = Ix[k]*Iy[k];
  }

  for (k=0; k<w*h; k++){
      J_Ix2[k]  = Ix2[k];
      J_Iy2[k]  = Iy2[k];
      J_IxIy[k] = IxIy[k];
    }


  for (i=0; i<w; i++)
    for (j=0; j<h; j++){
      int n=0;
      for(k=-p; k<=p; k++)
	for(int l=-p; l<=p; l++){
	  int i_=i+k;
	  int j_=j+l;
	  if (INCLUS(i_,j_,w,h)!=0){
	    int ind_=i_+w*j_;
	    J_Ix2[i+w*j] += Ix2[ind_];
	    J_Iy2[i+w*j] += Iy2[ind_];
	    J_IxIy[i+w*j] += IxIy[ind_];
	    n++;
	  }
	}
      J_Ix2[i+w*j]  /= n;
      J_Iy2[i+w*j]  /= n;
      J_IxIy[i+w*j] /= n;
    }


  /* Compute the eigenvalues and the Q and R metrics */
  /* ----------------------------------------------- */
  for (k=0; k<w*h; k++){
    j11 = J_Ix2[k];
    j22 = J_Iy2[k];
    j12 = J_IxIy[k];
    
    lambda1 = (j11+j22+sqrt((j11-j22)*(j11-j22)+4*j12*j12))/2.;
    lambda2 = (j11+j22-sqrt((j11-j22)*(j11-j22)+4*j12*j12))/2.;
    
    R[k] = (float)(lambda1-lambda2)/(lambda1+lambda2);
    Q[k] = (float)(R[k]); 
  }
  

  /*
  float *Qsave = new float[w*h]; 
  for (k=0; k<w*h; k++) 
    Qsave[k] = Q[k];
  
  for (i=0; i<w; i++)
    for (j=0; j<h; j++){
      float Qmax = Qsave[i+w*j];
      for(k=-p; k<=p; k++)
	for(int l=-p; l<=p; l++){
	  int i_=i+k;
	  int j_=j+l;
	  if (INCLUS(i_,j_,w,h)!=0){
	    int ind_=i_+w*j_;
	    if (Qsave[ind_]>Qmax)
	      Qmax = Qsave[ind_];
	  }
	}
      Q[i+w*j] = Qmax;
    }
  */

 
  
 float *Qsave = new float[w*h]; 
  for (k=0; k<w*h; k++) 
    Qsave[k] = Q[k];
  
  for (i=0; i<w; i++)
    for (j=0; j<h; j++){
      float sumQ = 0.;
      int   n = 0;
      for(k=-p; k<=p; k++)
	for(int l=-p; l<=p; l++){
	  int i_=i+k;
	  int j_=j+l;
	  if (INCLUS(i_,j_,w,h)!=0){
	    int ind_=i_+w*j_;
	    if (R[ind_]>0.234){
	      sumQ += Qsave[ind_];
	      n++;
	    }
	  }
	}
      if (n==0) Q[i+w*j];
      else      Q[i+w*j] = sumQ/n;
    }
  

}

/*==============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : Tensor
_Subject  :

-----------------------------------------------------------------------------*/

void Tensor(float *pixmap, int w, int h, int p, float *angle, float *anisotropy, float *valp1, float *valp2)
{

  int i, j, k;
  double lambda1, lambda2;
  double j11, j22, j12;



  /* Memory allocation */
  /* ----------------- */
  double *Ix      = new double[w*h]; 
  double *Iy      = new double[w*h]; 
  double *Ix2     = new double[w*h]; 
  double *Iy2     = new double[w*h]; 
  double *IxIy    = new double[w*h]; 
  double *J_Ix2   = new double[w*h]; 
  double *J_Iy2   = new double[w*h]; 
  double *J_IxIy  = new double[w*h]; 
 


  /* Compute the image gradients */
  /* --------------------------- */
  for(i=1; i<w-1; i++)
    for(j=1; j<h-1; j++){
      int r=i+w*j;
      Ix[r] = (10*(pixmap[r-1]-pixmap[r+1])+3*(pixmap[r-1-w]-pixmap[r+1-w])+3*(pixmap[r-1+w]-pixmap[r+1+w]))/4.;
      Iy[r] = (10*(pixmap[r-w]-pixmap[r+w])+3*(pixmap[r-w-1]-pixmap[r+w-1])+3*(pixmap[r-w+1]-pixmap[r+w+1]))/4.;
      //Ix[r] = pixmap[r]-pixmap[r+1];
      //Iy[r] = pixmap[r]-pixmap[r+w];
    }
  

  /* Compute the components of structure tensor */
  /* ------------------------------------------ */
  for (k=0; k<w*h; k++){
    Ix2[k]  = Ix[k]*Ix[k];
    Iy2[k]  = Iy[k]*Iy[k];
    IxIy[k] = Ix[k]*Iy[k];
  }

  for (k=0; k<w*h; k++){
      J_Ix2[k]  = Ix2[k];
      J_Iy2[k]  = Iy2[k];
      J_IxIy[k] = IxIy[k];
    }


  for (i=0; i<w; i++)
    for (j=0; j<h; j++){
      int n=0;
      for(k=-p; k<=p; k++)
	for(int l=-p; l<=p; l++){
	  int i_=i+k;
	  int j_=j+l;
	  if (INCLUS(i_,j_,w,h)!=0){
	    int ind_=i_+w*j_;
	    J_Ix2[i+w*j] += Ix2[ind_];
	    J_Iy2[i+w*j] += Iy2[ind_];
	    J_IxIy[i+w*j] += IxIy[ind_];
	    n++;
	  }
	}
      J_Ix2[i+w*j]  /= n;
      J_Iy2[i+w*j]  /= n;
      J_IxIy[i+w*j] /= n;
    }


   /* Compute the eigenvalues, angles and anisotropy */
  /* ---------------------------------------------- */
  for (k=0; k<w*h; k++){
    j11 = J_Ix2[k];
    j22 = J_Iy2[k];
    j12 = J_IxIy[k];
    
    lambda1 = (j11+j22+sqrt((j11-j22)*(j11-j22)+4*j12*j12))/2.;
    lambda2 = (j11+j22-sqrt((j11-j22)*(j11-j22)+4*j12*j12))/2.;

    float x1        = (j22-j11+sqrt((j11-j22)*(j11-j22)+4*j12*j12));
    float x2        = 2.*j12;
    float cos_theta = x1/sqrt(x1*x1+x2*x2);
    float sin_theta = x2/sqrt(x1*x1+x2*x2);
    float theta     = atan(sin_theta/cos_theta);

    if (cos_theta>0 && sin_theta<0)
      theta += 3.1415927;
    else
      if (cos_theta<0 && sin_theta>0)
	theta += 3.1415927;
      else
	if (sin_theta==1)
	  theta = 3.1415927/2;
	else
	  if (sin_theta==-1)
	    theta = 3.1415927/2;
	  else
	    if (cos_theta==1)
	      theta = 0;
	    else
	      if (cos_theta==-1)
		theta = 3.1415927;

      anisotropy[k] = (float)(lambda1-lambda2)/(lambda1+lambda2);
      angle[k]      = (float)(theta); 
      valp1[k]      = lambda1;
      valp2[k]      = lambda2;

      
  }

}

/*==============================================================================*/










/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : GaussFilter
_Subject  : 
-----------------------------------------------------------------------------*/

void GaussFilter(float variance, int *index, float *filter)
{
  int i;
  float	value;
  float sum_filter = 0.;

  *index = (int)(0.5+sqrt((double)(2*variance*log((double)300))));

  value = sqrt((double)(1/(2*PI*variance)));

  for (i=0; i<*index; i++) {
    filter[(*index-1)+i] = filter[(*index-1)-i] = value*exp((double)(-(float)(i*i)/2/variance));
  }

  for (i=0; i<2*(*index)-1; i++) sum_filter+=filter[i];
  for (i=0; i<2*(*index)-1; i++) filter[i]/=sum_filter;
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------
_Function_name 	: ApplyFilter2D
_Subject 	: Filtering of an image with borders effects processing
-----------------------------------------------------------------------------*/

void ApplyFilter2D(float *Iin, int w, int h, int index, float *filter, float *Iout)
{
  int ind = index-1;
  int i, j, k;
  float *Itemp, *pIr;
  float *Ior, *Ires;


  Itemp = new float [(w+2*ind)*(h+2*ind)];
  Ior   = new float [(w+2*ind)*(h+2*ind)]; 
  Ires  = new float [(w+2*ind)*(h+2*ind)]; 


  for (i=0; i<h; i++)
    for (j=0; j<w; j++)
      Ior[(i+ind)*(w+2*ind)+j+ind] = (float)(Iin[i*w+j]);

  for (i=ind; i>=0; i--)
    for (j=0; j<w; j++)
      Ior[i*(w+2*ind)+j+ind] = (float)(Iin[j]);

  for (i=h; i<h+2*ind; i++)
    for (j=0; j<w; j++)
      Ior[i*(w+2*ind)+j+ind] = (float)(Iin[(h-1)*w+j]);

  for (i=0; i<h+2*ind; i++)
    for (j=ind; j>=0; j--)
      Ior[i*(w+2*ind)+j] = Ior[i*(w+2*ind)+j+1];

  for (i=0; i<h+2*ind; i++)
    for (j=w; j<w+2*ind; j++)
      Ior[i*(w+2*ind)+j] = Ior[i*(w+2*ind)+j-1];


  h += 2*ind;
  w += 2*ind;

  pIr = Itemp;

  for (i=0; i<h*w; i++) *pIr++ = 0.;

  for (i=0; i<h; i++)
    for (j=ind; j<w-ind; j++)
      for (k=0; k<=2*ind; k++)
	if (j+k-ind>0 && j+k-ind<w)
	  *M_IJ(Itemp,w,i,j) += filter[k]*(*M_IJ(Ior,w,i,j+k-ind));

  for (i=ind; i<h-ind; i++)
    for (j=0; j<w; j++) {
      *M_IJ(Ires,w,i,j) = 0.;
      for (k=0; k<=2*ind; k++)
	if (i+k-ind>0 && i+k-ind<h)
	  *M_IJ(Ires,w,i,j) += filter[k]*(*M_IJ(Itemp,w,i+k-ind,j));
    }

  h -= 2*ind;
  w -= 2*ind;

  for (i=0; i<h; i++)
    for (j=0; j<w; j++)
      Iout[i*w+j] = (float)(Ires[(i+ind)*(w+2*ind)+j+ind]);

  delete[] Itemp; 
  delete[] Ior;
  delete[] Ires;  
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : GaussianFiltering
_Subject  :
-----------------------------------------------------------------------------*/

void GaussianFiltering(float *I, float sigma, float *Igaussian, int w, int h)
{
  int sizeGF;
  float filter[100];

  GaussFilter(sigma,&sizeGF,filter);
  printf("Standard deviation: %2.2f\n", sigma);
  printf("Filter size: %d\n", 2*sizeGF+1);
  ApplyFilter2D(I,w,h,sizeGF,filter,Igaussian); 
}

/*==============================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : MSE_Var_B2
_Subject  : MSE computation 
-----------------------------------------------------------------------------*/

float MSE_Var_B2(float *Inoise, float *DN_I, int w, int h, int SigBlur, float *MSE)
{
  int ind;
  int sizeGF;
  float mse=0.;

  float filter[1000];

  float *DN_I_mean  = new float[w*h];
  float *B          = new float[w*h];
  float *Var        = new float[w*h];
  
 
  GaussFilter(SigBlur*SigBlur,&sizeGF,filter);
  
  if (sizeGF>0.) ApplyFilter2D(DN_I,w,h,sizeGF,filter,DN_I_mean);

  for (ind=0; ind<w*h; ind++) Var[ind] = (DN_I[ind] - DN_I_mean[ind])*(DN_I[ind] - DN_I_mean[ind]);	    
  if (sizeGF>0.) ApplyFilter2D(Var,w,h,sizeGF,filter,Var);

  for (ind=0; ind<w*h; ind++) B[ind] = (DN_I[ind] - Inoise[ind]);
  if (sizeGF>0.) ApplyFilter2D(B,w,h,sizeGF,filter,B);

  printf("\n");

  for (ind=0; ind<w*h; ind++){
    MSE[ind] = Var[ind] + B[ind]*B[ind];
    if (ind>5000 && ind<5250) printf("%f %f | ", Var[ind], B[ind]*B[ind]);
    mse += MSE[ind];
  }

  printf("\n");
    
  return (mse/(w*h));
}

/*==============================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : MSE_Var_B2
_Subject  : MSE computation 
-----------------------------------------------------------------------------*/

float MSE_Var_B2_(float *In, float *Ie, int w, int h, int SigBlur, float *MSE)
{
	int ind;
	int sizeGF;
	float mse, bias2, var;
	float alpha = 0.5;
	float N = (float)(w*h);
	
	float filter[1000];
	
	float *Ie_mean = new float[w*h];
	float *Bias    = new float[w*h];
	float *Var     = new float[w*h];
	
	GaussFilter(SigBlur*SigBlur,&sizeGF,filter);
	if (sizeGF>0.) ApplyFilter2D(Ie,w,h,sizeGF,filter,Ie_mean);
	carte_MAD_gaussien(Ie,0.1,Var,7,w,h);
	
	float *R = new float[w*h];
	PseudoResidu(Ie,R,w,h);		
	float sig = SigmaLTS(R,w*h);
	float sig2 =sig*sig;
	
	//for (ind=0; ind<w*h; ind++) Bias[ind] = (Ie[ind] - In[ind])*(Ie[ind] - In[ind]);
	for (ind=0; ind<w*h; ind++) Bias[ind] = (Ie[ind] - In[ind]);
	//float b2 = BiasLTS(Bias,w*h);
	
	//if (sizeGF>0.) ApplyFilter2D(Bias,w,h,sizeGF,filter,Bias);
		
	carte_MAD_gaussien(Bias,0.1,Var,3,w,h);
 
	bias2 = 0.;
	var   = 0.;
	
	for (ind=0; ind<w*h; ind++){
	  bias2 += Bias[ind]*Bias[ind];
	  var   += Var[ind];
	  MSE[ind] =  fabs(sqrt(Var[ind])-20);
	  //MSE[ind] =  Var[ind] + Bias[ind]*Bias[ind];
	  //if (ind>5000 && ind<5250) printf("%f %f | ", Var[ind], Bias[ind]*Bias[ind]);
	  if (ind>5000 && ind<5250) printf("%f %f | ", sqrt(Var[ind]), MSE[ind]);
	}
    	
	var   /=N;
	bias2 /= N;
	mse    = bias2 + sig2;
	
	//printf("\nbias2=%f b2=%f\n", bias2, b2);
	//printf("mse=%f bias2=%f sig2=%f \n", mse, bias2, sig2);
	return (mse);
}

/*==============================================================================*/







/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : PeakFiltering
_Subject  :

-----------------------------------------------------------------------------*/

void PeakFiltering(float *pixmap, int w, int h, double alpha, float *nopeak_pixmap)
{

  int i, j;
  int p = 1;
  int Npeaks = 0;
  float mean, var, n; 

  float *Iout = new float[w*h];

  for (i=0; i<w; i++)
    for (j=0; j<h; j++){

      Iout[i+w*j] = pixmap[i+w*j];

      n = 0.;
      mean = 0.;
      for(int k=-p; k<=p; k++)
	for(int l=-p; l<=p; l++){
	  int i_=i+k;
	  int j_=j+l;
	  if (INCLUS(i_,j_,w,h)!=0){	    
	    mean += pixmap[i_+w*j_];
	    n    += 1.;
	  }
	}
      mean -=  pixmap[i+w*j];
      mean /= (n-1);
	
      n = 0.;
      var = 0.;
      for(int k=-p; k<=p; k++)
	for(int l=-p; l<=p; l++){
	  int i_=i+k;
	  int j_=j+l;
	  if (INCLUS(i_,j_,w,h)!=0){  
	    var += (pixmap[i_+w*j_] - mean)*(pixmap[i_+w*j_] - mean);
	    n    += 1.;
	  }
	}	    
	  
      var -=  (pixmap[i+w*j]-mean)*(pixmap[i+w*j]-mean);
      var /= (n-1);
	
      if (fabs(pixmap[i+w*j]-mean)>alpha*sqrt(var)){
	Iout[i+w*j] = mean;
	Npeaks++;
      }  
      
    }

  for (int ind=0; ind<w*h; ind++) nopeak_pixmap[ind] = Iout[ind];
  printf("Number of removed pixels: %d \n", Npeaks);

}

/*==============================================================================*/







/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : carte_NoiseVarianceML
_Subject  :

-----------------------------------------------------------------------------*/

void carte_NoisevarianceML(float *p_pixmap, float sig2, float *sig_I, int p, int w, int h, float gamma_p)
{
  int i, j, i_, j_, n, m;
  int card;
  int sampler=1;
  float sigl;
  float mean;

  for(i=0; i<w; i++){
    for(j=0; j<h; j++){

      card = 0;
      mean = 0.;
      for(n=-p; n<=p; n++)
	for(m=-p; m<=p; m++){
	  i_=i+n;
	  j_=j+m;
	  if (INCLUS(i_,j_,w,h)!=0){
	    if (((n+1)%sampler==0) && ((m+1)%sampler==0)){
	      mean += p_pixmap[i_+w*j_];
	      card++;
	    }
	  }
	}
      mean /= card;

      for(n=-p; n<=p; n++)
	for(m=-p; m<=p; m++){
	  i_=i+n;
	  j_=j+m;
	  if (INCLUS(i_,j_,w,h)!=0){
	    if (((n+1)%sampler==0) && ((m+1)%sampler==0)){
	      sigl = (p_pixmap[i_+w*j_] - mean)*(p_pixmap[i_+w*j_] - mean);
	    }
	  }
	}

      sig_I[i+w*j] = sigl/card;
      sig_I[i+w*j] = gamma_p*MAX(sig2,sig_I[i+w*j]);
    }
  }

  
  //int sizeGF;
  //float filter[1000];
  //GaussFilter(2.,&sizeGF,filter);
  //ApplyFilter2D(sig_I,w,h,sizeGF,filter,sig_I); 
  //for (i=0; i<w*h; i++) sig_I[i] *= gamma_p; 
    
}

/*==============================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : carte_NoiseVariance_PoissonGaussien
_Subject  :

-----------------------------------------------------------------------------*/

void carte_NoiseVariance_PoissonGaussien(float *p_pixmap, float g0, float e_DC, float *sig_I, int p, int w, int h)
{
  int i, j, i_, j_, n, m;
  int card;
  int sampler=1;
  float sigl;
  float mean;
  float med;
  float *T = new float [625];

  for(i=0; i<w; i++){
    for(j=0; j<h; j++){

      card = 0;
      mean = 0.;
      for(n=-p; n<=p; n++)
	for(m=-p; m<=p; m++){
	  i_=i+n;
	  j_=j+m;
	  if (INCLUS(i_,j_,w,h)!=0){
	    if (((n+1)%sampler==0) && ((m+1)%sampler==0)){
	      T[card] = p_pixmap[i_+w*j_];
	      mean += p_pixmap[i_+w*j_];
	      card++;
	    }
	  }
	}
      mean /= card;

      Tri(T,card);
      med = T[(int)(card/2.)];

      //sig_I[i+w*j] = g0*mean+e_DC;
      sig_I[i+w*j] = g0*med+e_DC;
     }
  }    
}

/*==============================================================================*/








/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : WildBootstap
_Subject  : Wild Bootstrap 
-----------------------------------------------------------------------------*/

void WildBootstrap(float *I0, float *Istar, float *Istar_, int w, int h)
{
  int i;
  double r1, r2;
  double V, V2;
  float *Err = new float[w*h];

  for (i=0; i<w*h; i++){
    Err[i] = I0[i] - Istar[i];
    
    r1 = rand()/(float)(RAND_MAX);
    r2 = rand()/(float)(RAND_MAX);
    V = sqrt(-2*log((double)(1e-10 + (1-2e-10)*r1)))*cos((double)(2*PI*r2));
    
    Istar_[i] = Istar[i] + Err[i]*(V/sqrt(2.)+(V*V-1.)/2.); 
    //Istar_[i] = MIN(255.,MAX(0.,Istar_[i]));
  }
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : WGNoise
_Subject  : WGNoise 
-----------------------------------------------------------------------------*/

void WGNoise(float *I, float *Iwgn, float *noise, int w, int h, float sigma, float eps)
{
  int i;
  double r1, r2;
  double V;

  for (i=0; i<w*h; i++){
    r1 = rand()/(float)(RAND_MAX);
    r2 = rand()/(float)(RAND_MAX);
    V  = sigma*sqrt(-2*log((double)(1e-10 + (1-2e-10)*r1)))*cos((double)(2*PI*r2));
    
    noise[i] = V;
    Iwgn[i]  = I[i] + eps*V; 
  }
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : comp_div
_Subject  : comp_div 
-----------------------------------------------------------------------------*/

float comp_div(float *I, float *Ieps, float *noise, int w, int h, float eps)
{
  int i;
  float div=0.;

  for (i=0; i<w*h; i++) div += noise[i]*(Ieps[i]-I[i]);

  return(div/eps);
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : cov_I
_Subject  :

-----------------------------------------------------------------------------*/

void cov_I(float *ima_u, float *ima_v, int p, int w, int h, float *ima_cov)
{
  int i, j, i_, j_, n, m;
  float card;
  float mean_u, mean_v;
  float cov;
  

  for(i=0; i<w; i++)
    for(j=0; j<h; j++){
      card   = 0.;
      mean_u = 0.;
      mean_v = 0.;
      
      for(n=-p; n<=p; n++)
	for(m=-p; m<=p; m++){
	  i_=i+n;
	  j_=j+m;
	  if (INCLUS(i_,j_,w,h)!=0){
	    mean_u += ima_u[i_+w*j_];
	    mean_v += ima_v[i_+w*j_];
	    card   += 1.;
	  }
	}
      mean_u /= card;
      mean_v /= card;
      //printf(" %f  %f \n", mean_u, mean_v);
      
      cov  = 0.;
      for(n=-p; n<=p; n++)
	for(m=-p; m<=p; m++){
	  i_=i+n;
	  j_=j+m;
	  if (INCLUS(i_,j_,w,h)!=0) 
	    cov += (ima_u[i_+w*j_]-mean_u)*(ima_v[i_+w*j_]-mean_v);
	}
      ima_cov[i+w*j] = cov/card;
      
    }
}

/*==============================================================================*/





