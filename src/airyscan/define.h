/*=============================================================================

_Header

_Module_Name 	: define.h

_Directory	: 

_Description 	: Definition de define.h

_Calls   	:

_Authors	: Charles KERVRANN

_Contents       :

_Last Release   : 17/05/07

_end

=============================================================================*/

#define SQR(X)           (X*X)
#define ABSX(X)          ((X)<(0) ? (-X):(X))
#define ABSXY(X,Y)       ((X-Y)<(0) ? (Y-X) : (X-Y))
#define MAX(X,Y)         ((X)>(Y) ? (X) : (Y))
#define MIN(X,Y)         ((X)<(Y) ? (X) : (Y))
#define INCLUS(X,Y,W,H)  (((X)>(-1))&&((X)<(W))&&((Y)>(-1))&&((Y)<(H)) ? 1 : 0)
#define CUT(X)           ((X)<(0) ? (0) : (X)>(255) ? (255) : (X))
#define IND(X,S)         ((X-S)>(0) ? (1) : (0))
#define M_IJ(VAL,W,Y,X)  ((VAL) + ((Y) * (W)) + (X))

#define IMAX             64.
#define INTMAX           256
#define PI               3.1415927
#define FORMEGOMMETTE    'l'
#define EPSDETECT        0.25
#define WSUP             -1
#define ADAPTIVESIGMA    -1
#define EPS_0            1e-10

/*===========================================================================*/
