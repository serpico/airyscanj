/// \file StackSure.h
/// \brief StackSure class
/// \author Sylvain Prigent
/// \version 0.1
/// \date 2020

#pragma once

#include <string>
#include <vector>

#include "airyscanExport.h"
#include "simage/SImageFloat.h"


/// \class StackSure
/// \brief class that contains the implementation of SPARTION 2D deconvolution
class AIRYSCAN_EXPORT StackSure{

public:
    StackSure(SImageFloat* stack, unsigned int ref_idx, unsigned int m1, unsigned int m2, unsigned int n1, unsigned int n2);

public:
    void setWindowSize(int value);
    void run();

public:
    SImageFloat* getEpsilonMap();

protected:
    SImageFloat* m_epsilonMap;

protected:
    SImageFloat* m_stack;
    unsigned int m_ref_idx;
    unsigned int m_m1;
    unsigned int m_m2;
    unsigned int m_n1;
    unsigned int m_n2;
    int m_windowSize;
};
