# 	Airyscan

Set of algorithms to reconstruct Airyscan high resolution images from raw data (32 detectors) from Airyscan microscopes.

The repository contains Fiji macros and c++ command line tools.
  

## Create a Fiji package from sources

1. Create a `AiryscanJ` directory in the `Fiji/plugins` directory
2. Copy the content from the `macros` folder to the `AiryscanJ` folder
3. compile the c++ code: 
```
cd airyscan
mkdir build
cmake ..
make
```
4. copy the binary `sure` to the `AiryscanJ` folder
5. Compile the deconvolution algorithm: follow the instructions at https://gitlab.inria.fr/serpico/spartion
6. Copy the `sdeconv2d` binary to the `AiryscanJ` folder
7. Compile the documentation:
```
cd docs
mkdir build
sphinx-build -b html ./source ./build
``` 
8. Copy the content of the `docs/build` folder to `AiryscanJ/doc`
