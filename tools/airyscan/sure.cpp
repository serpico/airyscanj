#include <iostream>
#include <score>
#include <scli>
#include <simageio>
#include <airyscan>

int main(int argc, char *argv[])
{
    SObserverConsole* observer = new SObserverConsole();
    try
    {
        SCliParser cmdParser(argc, argv);
        cmdParser.addInputData("-i", "Input image file");
        cmdParser.addOutputData("-o", "Output image file");

        cmdParser.addParameterInt("-refidx", "Index of the reference frame", 0);
        cmdParser.addParameterInt("-m1", "a first index", 0);
        cmdParser.addParameterInt("-m2", "a second index", 31);
        cmdParser.addParameterInt("-n1", "b first index", 0);
        cmdParser.addParameterInt("-n2", "b second index", 31);
        cmdParser.addParameterInt("-neigh", "size of the neigboring window", 11);

        cmdParser.setMan("Calculate the epsilon map for SURE parameter estimation of Airyscan reconstruction");
        cmdParser.parse(2);


        std::string inputImageFile = cmdParser.getDataURI("-i");
        std::string outputImageFile = cmdParser.getDataURI("-o");

        const int refidx = cmdParser.getParameterInt("-refidx");
        const int m1 = cmdParser.getParameterInt("-m1");
        const int m2 = cmdParser.getParameterInt("-m2");
        const int n1 = cmdParser.getParameterInt("-n1");
        const int n2 = cmdParser.getParameterInt("-n2");
        const int neigh = cmdParser.getParameterInt("-neigh");


        if (inputImageFile == ""){
            observer->message("Sure: Input image path is empty");
            return 1;
        }

        observer->message("Sure: input image: " + inputImageFile);
    
        // Run process
        SImageFloat* inputImage = dynamic_cast<SImageFloat*>(SImageReader::read(inputImageFile, 32));
        if (inputImage->getSizeZ() != 32){
            observer->message("Sure: can only process images with 32 frames");
        }

        StackSure process(inputImage, refidx, m1, m2, n1, n2);
        process.setWindowSize(neigh);
        process.run();

        // save outputs
        SImage* outputImage = process.getEpsilonMap();
        SImageReader::write(outputImage, outputImageFile);

        delete inputImage;
        delete outputImage;
    }
    catch (SException &e)
    {
        observer->message(e.what(), SObserver::MessageTypeError);
        return 1;
    }
    catch (std::exception &e)
    {
        observer->message(e.what(), SObserver::MessageTypeError);
        return 1;
    }
    delete observer;
    return 0;
}
